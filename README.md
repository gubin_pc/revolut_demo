### How to run
Call next command and start application on 8081 port:

```bash
./mvnw clean package && java -jar -Dmoney.transfer.server.port=8081  target/revolut_demo-1.0.0-jar-with-dependencies.jar
```
By default application use 4567 port. Change System Property `money.transfer.server.port` that change port.

### API Description
#### Create new account with name
```
POST /accounts
Content-Type: application/json

{
  "name": "test"
}
```

#### Get account info
```
GET /accounts/{accountId}
```

#### Charge some amount
```
POST /accounts/{accountId}/operations/charge
Content-Type: application/json

12.255
```

#### Withdraw some amount
```
POST /accounts/{accountId}/operations/withdraw
Content-Type: application/json

2.255
```

#### Transfer some amount betwen two accounts
```
POST /accounts/{accountId}/operations/transfer
Content-Type: application/json

{
  "accountId": 2,
  "amount": 5
}
```
