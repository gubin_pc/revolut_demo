package com.revolut.demo.services.moneytransfers.service.impl;

import com.revolut.demo.services.moneytransfers.model.Account;
import com.revolut.demo.services.moneytransfers.model.BalanceHistory;
import com.revolut.demo.services.moneytransfers.service.AccountOperationService;
import com.revolut.demo.services.moneytransfers.storage.AccountInMemoryStorage;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.NoSuchElementException;
import java.util.function.Consumer;

public class AccountOperationServiceImpl implements AccountOperationService {

    @Override
    public void charge(int accountId, @NotNull BigDecimal amount) {
        lockAndUpdate(accountId, account -> {
            increaseBalance(account, amount);
            account.getBalance().addBalanceHistory(
                    new BalanceHistory(amount,  Instant.now(),"Balance was increase for " + amount.toString())
            );
        });
    }

    @Override
    public void withdraw(int accountId, @NotNull BigDecimal amount) {
        lockAndUpdate(accountId, account -> {
            decreaseBalance(account, amount);
            account.getBalance().addBalanceHistory(
                    new BalanceHistory(amount.negate(), Instant.now(), "Balance was decrease for " + amount.toString())
            );
        });
    }

    @Override
    public void transfer(int accountIdSource, int accountIdDestination, @NotNull BigDecimal amount) {
        if (accountIdSource == accountIdDestination) {
            throw new IllegalArgumentException("Can not transfer for same account");
        } else if (accountIdSource < accountIdDestination) {
            lockAndUpdate(accountIdSource, sourceAccountData ->
                    lockAndUpdate(accountIdDestination, destinationAccountData ->
                            transfer(sourceAccountData, destinationAccountData, amount)));
        } else {
            lockAndUpdate(accountIdDestination, destinationAccountData ->
                    lockAndUpdate(accountIdSource, sourceAccountData ->
                            transfer(sourceAccountData, destinationAccountData, amount)));
        }
    }

    private void increaseBalance(Account account, BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Balance can not be increase for negative amount");
        }
        BigDecimal oldBalance = account.getBalance().getBalanceAmount();
        account.getBalance().setBalance(oldBalance.add(amount));
    }

    private void decreaseBalance(Account account, BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Balance can not be decrease for negative amount");
        }

        BigDecimal oldBalance = account.getBalance().getBalanceAmount();
        BigDecimal restOfBalance = oldBalance.subtract(amount);

        if (restOfBalance.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Balance after decrease can not be less than zero");
        }
        account.getBalance().setBalance(restOfBalance);
    }

    private void transfer(Account sourceAccount, Account destinationAccount, BigDecimal amount) {
        decreaseBalance(sourceAccount, amount);
        increaseBalance(destinationAccount, amount);
        Instant now = Instant.now();
        sourceAccount.getBalance().addBalanceHistory(new BalanceHistory(amount.negate(), now,
                "Send " + amount.toString() + " to account #" + destinationAccount.getAccountId()));
        destinationAccount.getBalance().addBalanceHistory(new BalanceHistory(amount, now,
                "Get " + amount.toString() + " from account #" + sourceAccount.getAccountId()));
    }

    public static void lockAndUpdate(int accountId, @NotNull Consumer<Account> bucketConsumer) {
        Account account = AccountInMemoryStorage.find(accountId);
        if (account != null) {
            try {
                account.getLock().writeLock().lock();
                bucketConsumer.accept(account);
                return;
            } finally {
                account.getLock().writeLock().unlock();
            }
        }
        throw new NoSuchElementException("AccountId [" + accountId + "] not found");
    }


}
