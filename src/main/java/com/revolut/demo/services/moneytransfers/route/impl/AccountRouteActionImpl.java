package com.revolut.demo.services.moneytransfers.route.impl;

import com.revolut.demo.services.moneytransfers.route.AccountRouteAction;
import com.revolut.demo.services.moneytransfers.service.AccountService;
import org.jetbrains.annotations.NotNull;
import spark.Route;

import java.util.Optional;

public class AccountRouteActionImpl implements AccountRouteAction {

    private final AccountService accountService;

    public AccountRouteActionImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    @NotNull
    @Override
    public Route createAccount() {
        return (request, response) -> {
            response.status(201); //201 Created
            return accountService.createAccount(request.body());
        };
    }

    @NotNull
    @Override
    public Route getAccount() {
        return (request, response) -> Optional.ofNullable(request.params(":accountId"))
                .map(this::parseInt)
                .map(accountService::getAccount)
                .orElseThrow(() -> new IllegalArgumentException("accountId not define"));
    }

    private int parseInt(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Account id should be Integer, but was [" + str + "]");
        }
    }

}
