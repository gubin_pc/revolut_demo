package com.revolut.demo.services.moneytransfers.service;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;

public interface AccountOperationService {
    void charge(int accountId, @NotNull BigDecimal amount);
    void withdraw(int accountId, @NotNull BigDecimal amount);
    void transfer(int accountIdSource, int accountIdDestination, @NotNull BigDecimal amount);
}
