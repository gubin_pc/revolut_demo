package com.revolut.demo.services.moneytransfers;

import com.revolut.demo.services.moneytransfers.route.AccountOperationRouteAction;
import com.revolut.demo.services.moneytransfers.route.AccountRouteAction;
import com.revolut.demo.services.moneytransfers.support.BeansConfigurationContainer;
import com.revolut.demo.services.moneytransfers.support.SparkUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ResponseTransformer;
import spark.Spark;

import java.util.NoSuchElementException;

import static spark.Spark.*;

public class MoneyTransferApplication {

    private static final Logger log = LoggerFactory.getLogger(MoneyTransferApplication.class);
    private static final String APPLICATION_JSON = "application/json";

    static {
        SparkUtils.setPort(System.getProperty("money.transfer.server.port"));
        SparkUtils.createServerWithRequestLog(log);
        Runtime.getRuntime().addShutdownHook(new Thread(Spark::stop));
    }

    public static void main(String[] args) {

        AccountRouteAction accountRouteAction = BeansConfigurationContainer.getBean(AccountRouteAction.class);
        AccountOperationRouteAction accountOperationRouteAction = BeansConfigurationContainer.getBean(AccountOperationRouteAction.class);
        ResponseTransformer responseTransformer = BeansConfigurationContainer.getBean(ResponseTransformer.class);

        path("/accounts", () -> {
            post("", accountRouteAction.createAccount());
            get("/:accountId", accountRouteAction.getAccount(), responseTransformer);
            path("/:accountId/operations", () -> {
                post("/charge", accountOperationRouteAction.charge());
                post("/withdraw", accountOperationRouteAction.withdraw());
                post("/transfer", accountOperationRouteAction.transfer());
            });
        });
        after((request, response) -> response.type(APPLICATION_JSON));
        exception(IllegalArgumentException.class, (exception, request, response) -> {
            response.status(400); //400 Bad Request
            response.body(exception.getMessage());
            log.warn(exception.getMessage());
        });
        exception(NoSuchElementException.class, (exception, request, response) -> {
            response.status(404); //404 Not Found
            response.body(exception.getMessage());
            log.warn(exception.getMessage());
        });
        exception(Exception.class, (exception, request, response) -> {
            response.status(500); //500 Internal Server Error
            response.body("Internal service error");
            log.error(exception.getMessage(), exception);
        });

    }

}
