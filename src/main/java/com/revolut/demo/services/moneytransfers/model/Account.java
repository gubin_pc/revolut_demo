package com.revolut.demo.services.moneytransfers.model;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Account {
    private final int accountId;
    private final String name;
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final Balance balance = new Balance();

    public Account(int integer, String name) {
        this.accountId = integer;
        this.name = name;
    }

    public int getAccountId() {
        return accountId;
    }

    public String getName() {
        return name;
    }

    public Balance getBalance() {
        return balance;
    }

    public ReadWriteLock getLock() {
        return lock;
    }
}