package com.revolut.demo.services.moneytransfers.model.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revolut.demo.services.moneytransfers.model.Account;
import com.revolut.demo.services.moneytransfers.model.Balance;

import java.util.concurrent.locks.ReadWriteLock;


public abstract class AccountMixIn extends Account {

    public AccountMixIn(int integer, String name) {
        super(integer, name);
    }

    @JsonProperty(value = "accountId")
    public abstract int getAccountId();

    @JsonProperty(value = "name")
    public abstract String getName();

    @JsonProperty(value = "balance")
    public abstract Balance getBalance();

    @Override
    @JsonIgnore
    public abstract ReadWriteLock getLock();
}
