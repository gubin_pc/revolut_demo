package com.revolut.demo.services.moneytransfers.route.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.demo.services.moneytransfers.model.json.TransferRequest;
import com.revolut.demo.services.moneytransfers.route.AccountOperationRouteAction;
import com.revolut.demo.services.moneytransfers.service.AccountOperationService;
import org.jetbrains.annotations.NotNull;
import spark.Route;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

public class AccountOperationRouteActionImpl implements AccountOperationRouteAction {

    private final AccountOperationService accountOperationService;
    private final ObjectMapper objectMapper;

    public AccountOperationRouteActionImpl(AccountOperationService accountOperationService,
                                           ObjectMapper objectMapper) {
        this.accountOperationService = accountOperationService;
        this.objectMapper = objectMapper;
    }

    @NotNull
    @Override
    public Route charge() {
        return (request, response) -> {
            int accountId = parseInt(request.params(":accountId"));
            BigDecimal amount = parseBigDecimal(request.body());
            accountOperationService.charge(accountId, amount);
            return "";
        };
    }

    @NotNull
    @Override
    public Route withdraw() {
        return (request, response) -> {
            int accountId = parseInt(request.params(":accountId"));
            BigDecimal amount = parseBigDecimal(request.body());
            accountOperationService.withdraw(accountId, amount);
            return "";
        };
    }

    @NotNull
    @Override
    public Route transfer() {
        return (request, response) -> {
            int accountIdSource = parseInt(request.params(":accountId"));
            TransferRequest transferRequest = objectMapper.readValue(request.bodyAsBytes(), TransferRequest.class);
            accountOperationService.transfer(accountIdSource, transferRequest.getAccountId(), transferRequest.getAmount());
            return "";
        };
    }

    private static int parseInt(String str) {
        return Optional.ofNullable(str)
                .map(s -> {
                    try {
                        return Integer.parseInt(str);
                    } catch (NumberFormatException e) {
                        throw new IllegalArgumentException("Account id should be Integer, but was [" + str + "]");
                    }
                })
                .orElseThrow(() -> new IllegalArgumentException("accountId not define"));
    }

    private static BigDecimal parseBigDecimal(String bigDecimal) {
        return Optional.ofNullable(bigDecimal)
                .map(s -> {
                    try {
                        return new BigDecimal(bigDecimal).setScale(2, RoundingMode.HALF_UP);
                    } catch (NumberFormatException e) {
                        throw new IllegalArgumentException("Amount should be Number, but was [" + bigDecimal + "]");
                    }
                })
                .orElseThrow(() -> new IllegalArgumentException("Charge amount not define"));

    }

}
