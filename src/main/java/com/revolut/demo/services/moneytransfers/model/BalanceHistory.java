package com.revolut.demo.services.moneytransfers.model;

import java.math.BigDecimal;
import java.time.Instant;

public class BalanceHistory {
    private final BigDecimal amount;
    private final Instant operationTime;
    private final String description;

    public BalanceHistory(BigDecimal amount, Instant operationTime, String description) {
        this.amount = amount;
        this.operationTime = operationTime;
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Instant getOperationTime() {
        return operationTime;
    }

    public String getDescription() {
        return description;
    }

}
