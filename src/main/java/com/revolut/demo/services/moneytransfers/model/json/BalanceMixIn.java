package com.revolut.demo.services.moneytransfers.model.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.revolut.demo.services.moneytransfers.model.Balance;
import com.revolut.demo.services.moneytransfers.model.BalanceHistory;

import java.math.BigDecimal;
import java.util.List;

public abstract class BalanceMixIn extends Balance {

    @JsonProperty("balance")
    public abstract BigDecimal getBalanceAmount();

    @JsonProperty("balanceHistories")
    public abstract List<BalanceHistory> getBalanceHistories();
}
