package com.revolut.demo.services.moneytransfers.route;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Route;

import java.util.function.Function;

public interface AccountRouteAction {
    @NotNull Route createAccount();
    @NotNull Route getAccount();
}
