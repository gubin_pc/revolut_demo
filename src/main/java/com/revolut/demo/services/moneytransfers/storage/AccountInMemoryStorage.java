package com.revolut.demo.services.moneytransfers.storage;

import com.revolut.demo.services.moneytransfers.model.Account;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class AccountInMemoryStorage {

    private static AtomicInteger idCount = new AtomicInteger(0);

    private static final Map<Integer, Account> storage = new ConcurrentHashMap<>();


    public static int add(@NotNull String name) {
        int id = idCount.incrementAndGet();
        storage.put(id, new Account(id, name));
        return id;
    }

    public static Account find(int accountId) {
        return Optional.ofNullable(storage.get(accountId))
                .orElseThrow(() ->new NoSuchElementException("AccountId [" + accountId + "] not found"));
    }

}
