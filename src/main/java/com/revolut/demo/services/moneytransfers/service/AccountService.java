package com.revolut.demo.services.moneytransfers.service;

import com.revolut.demo.services.moneytransfers.model.Account;
import org.jetbrains.annotations.NotNull;

public interface AccountService {
    int createAccount(@NotNull String name);
    @NotNull Account getAccount(int accountId);
}
