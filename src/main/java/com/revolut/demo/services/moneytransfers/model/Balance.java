package com.revolut.demo.services.moneytransfers.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Balance {
    private BigDecimal balance = BigDecimal.ZERO;
    private final List<BalanceHistory> balanceHistories = new ArrayList<>();

    public BigDecimal getBalanceAmount() {
        return balance;
    }

    public List<BalanceHistory> getBalanceHistories() {
        return Collections.unmodifiableList(balanceHistories);
    }

    public void addBalanceHistory(BalanceHistory balanceHistory) {
        balanceHistories.add(balanceHistory);
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
