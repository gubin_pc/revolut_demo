package com.revolut.demo.services.moneytransfers.service.impl;

import com.revolut.demo.services.moneytransfers.model.Account;
import com.revolut.demo.services.moneytransfers.service.AccountService;
import com.revolut.demo.services.moneytransfers.storage.AccountInMemoryStorage;
import org.eclipse.jetty.util.StringUtil;
import org.jetbrains.annotations.NotNull;

public class AccountServiceImpl implements AccountService {

    @Override
    public int createAccount(@NotNull String name) {
        if (StringUtil.isBlank(name)) {
            throw new IllegalArgumentException("Account name can not be null or empty");
        }
        return AccountInMemoryStorage.add(name);
    }

    @Override
    public @NotNull Account getAccount(int accountId) {
        return AccountInMemoryStorage.find(accountId);
    }
}
