package com.revolut.demo.services.moneytransfers.route;

import org.jetbrains.annotations.NotNull;
import spark.Route;

public interface AccountOperationRouteAction {
    @NotNull Route charge();
    @NotNull Route withdraw();
    @NotNull Route transfer();
}
