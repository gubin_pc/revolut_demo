package com.revolut.demo.services.moneytransfers.model.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class TransferRequest {
    private final int accountId;
    private final BigDecimal amount;

    @JsonCreator
    public TransferRequest(@JsonProperty("accountId") int accountId,
                           @JsonProperty("amount") BigDecimal amount) {
        this.accountId = accountId;
        this.amount = amount;
    }

    public int getAccountId() {
        return accountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
