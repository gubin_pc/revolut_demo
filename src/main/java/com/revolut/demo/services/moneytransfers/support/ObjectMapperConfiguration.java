package com.revolut.demo.services.moneytransfers.support;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.revolut.demo.services.moneytransfers.model.Account;
import com.revolut.demo.services.moneytransfers.model.Balance;
import com.revolut.demo.services.moneytransfers.model.BalanceHistory;
import com.revolut.demo.services.moneytransfers.model.json.AccountMixIn;
import com.revolut.demo.services.moneytransfers.model.json.BalanceHistoryMixIn;
import com.revolut.demo.services.moneytransfers.model.json.BalanceMixIn;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ObjectMapperConfiguration {

    public static ObjectMapper getNewInstant() {
        return new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .disable(MapperFeature.USE_GETTERS_AS_SETTERS)
                .setMixIns(mixInConfiguration());
    }

    public static Map<Class<?>, Class<?>> mixInConfiguration() {
        Map<Class<?>, Class<?>> mixIns = new HashMap<>();
        mixIns.put(Account.class, AccountMixIn.class);
        mixIns.put(Balance.class, BalanceMixIn.class);
        mixIns.put(BalanceHistory.class, BalanceHistoryMixIn.class);
        return Collections.unmodifiableMap(mixIns);
    }
}
