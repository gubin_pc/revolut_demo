package com.revolut.demo.services.moneytransfers.model.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revolut.demo.services.moneytransfers.model.BalanceHistory;

import java.math.BigDecimal;
import java.time.Instant;

public abstract class BalanceHistoryMixIn extends BalanceHistory {

    @JsonCreator
    public BalanceHistoryMixIn(@JsonProperty("amount") BigDecimal amount,
                               @JsonProperty("operationTime") Instant operationTime,
                               @JsonProperty("description") String description) {
        super(amount, operationTime, description);
    }

    @JsonProperty("amount")
    public abstract BigDecimal getAmount();

    @JsonProperty("operationTime")
    public abstract Instant getOperationTime();

    @JsonProperty("description")
    public abstract String getDescription();
}
