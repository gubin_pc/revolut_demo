package com.revolut.demo.services.moneytransfers.support;

import com.revolut.demo.services.moneytransfers.MoneyTransferApplication;
import org.eclipse.jetty.server.CustomRequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.util.thread.ThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Spark;
import spark.embeddedserver.EmbeddedServers;
import spark.embeddedserver.jetty.EmbeddedJettyFactory;
import spark.embeddedserver.jetty.JettyServerFactory;

public class SparkUtils {

    private static final Logger log = LoggerFactory.getLogger(MoneyTransferApplication.class);

    public static void setPort(String port) {
        if (port != null) {
            try {
                Spark.port(Integer.parseInt(port));
            } catch (NumberFormatException e) {
                log.warn("port is not valid format");
            }
        }
    }

    public static void createServerWithRequestLog(Logger logger) {
        EmbeddedServers.add(EmbeddedServers.Identifiers.JETTY, new EmbeddedJettyFactory(new JettyServerFactory() {
            @Override
            public Server create(int maxThreads, int minThreads, int threadTimeoutMillis) {
                Server server;

                if (maxThreads > 0) {
                    int max = maxThreads;
                    int min = (minThreads > 0) ? minThreads : 8;
                    int idleTimeout = (threadTimeoutMillis > 0) ? threadTimeoutMillis : 60000;

                    server = new Server(new QueuedThreadPool(max, min, idleTimeout));
                } else {
                    server = new Server();
                }

                server.setRequestLog(new CustomRequestLog(logger::info, CustomRequestLog.NCSA_FORMAT));

                return server;
            }

            @Override
            public Server create(ThreadPool threadPool) {
                return threadPool != null ? new Server(threadPool) : new Server();
            }
        }));
    }

}