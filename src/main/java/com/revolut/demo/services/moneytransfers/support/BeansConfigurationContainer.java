package com.revolut.demo.services.moneytransfers.support;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.demo.services.moneytransfers.route.AccountOperationRouteAction;
import com.revolut.demo.services.moneytransfers.route.AccountRouteAction;
import com.revolut.demo.services.moneytransfers.route.JsonTransformer;
import com.revolut.demo.services.moneytransfers.route.impl.AccountOperationRouteActionImpl;
import com.revolut.demo.services.moneytransfers.route.impl.AccountRouteActionImpl;
import com.revolut.demo.services.moneytransfers.service.AccountOperationService;
import com.revolut.demo.services.moneytransfers.service.AccountService;
import com.revolut.demo.services.moneytransfers.service.impl.AccountOperationServiceImpl;
import com.revolut.demo.services.moneytransfers.service.impl.AccountServiceImpl;
import spark.ResponseTransformer;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public final class BeansConfigurationContainer {

    private static final Map<Class<?>, Object> BEANS_MAP = new HashMap<>();

    static {
        ObjectMapper objectMapper = ObjectMapperConfiguration.getNewInstant();
        AccountService accountService = new AccountServiceImpl();
        AccountOperationService accountOperationService = new AccountOperationServiceImpl();
        AccountRouteAction accountRouteAction = new AccountRouteActionImpl(accountService);
        AccountOperationRouteAction accountOperationRouteAction = new AccountOperationRouteActionImpl(accountOperationService, objectMapper);
        ResponseTransformer jsonTransformer = new JsonTransformer(objectMapper);

        register(AccountRouteAction.class, accountRouteAction);
        register(AccountOperationRouteAction.class, accountOperationRouteAction);
        register(ResponseTransformer.class, jsonTransformer);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBean(Class<T> type) throws NoSuchElementException {
        T t = (T) BEANS_MAP.get(type);
        if (t == null) {
            throw new NoSuchElementException("Bean for " + type.getTypeName() + "not found");
        }
        return t;
    }

    private static <T> void register(Class<T> type, T bean) {
        BEANS_MAP.put(type, bean);
    }

}
