package com.revolut.demo.services.moneytransfers;

import com.revolut.demo.services.moneytransfers.model.Account;
import com.revolut.demo.services.moneytransfers.support.AssertSupport;
import com.revolut.demo.services.moneytransfers.support.MoneyTransferRestClient;
import org.junit.BeforeClass;
import org.junit.Test;
import spark.Spark;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;

public class TransferApplication_Success_Test {

    private static final MoneyTransferRestClient moneyTransferRestClient = new MoneyTransferRestClient();
    private static int ONE;
    private static int TWO;

    @BeforeClass
    public static void beforeClass() throws Exception {
        MoneyTransferApplication.main(null);
        Spark.awaitInitialization();
        ONE = moneyTransferRestClient.createAccount("one");
        TWO = moneyTransferRestClient.createAccount("two");

        moneyTransferRestClient.charge(String.valueOf(ONE), new BigDecimal(500));
        moneyTransferRestClient.charge(String.valueOf(TWO), new BigDecimal(500));
    }

    @Test
    public void getAccount() throws IOException, URISyntaxException {
        Account accountOne = moneyTransferRestClient.getAccount(String.valueOf(ONE));
        Account accountTwo = moneyTransferRestClient.getAccount(String.valueOf(TWO));

        AssertSupport.assertAfterCreate(ONE, "one", accountOne);
        AssertSupport.assertAfterCreate(TWO, "two", accountTwo);
    }

    @Test
    public void chargeOperation() throws IOException, URISyntaxException {
        moneyTransferRestClient.charge(String.valueOf(ONE), new BigDecimal("0.1"));
        moneyTransferRestClient.charge(String.valueOf(TWO), new BigDecimal("1.1"));

        AssertSupport.assertBalance(moneyTransferRestClient.getAccount(String.valueOf(ONE)).getBalance());
        AssertSupport.assertBalance(moneyTransferRestClient.getAccount(String.valueOf(TWO)).getBalance());
    }

    @Test
    public void withdrawOperation() throws IOException, URISyntaxException {
        moneyTransferRestClient.withdraw(String.valueOf(ONE), new BigDecimal("0.1"));
        moneyTransferRestClient.withdraw(String.valueOf(TWO), new BigDecimal("1.1"));

        AssertSupport.assertBalance(moneyTransferRestClient.getAccount(String.valueOf(ONE)).getBalance());
        AssertSupport.assertBalance(moneyTransferRestClient.getAccount(String.valueOf(TWO)).getBalance());
    }

    @Test
    public void transferOperation() throws IOException, URISyntaxException {
        moneyTransferRestClient.transfer(String.valueOf(ONE), String.valueOf(TWO), BigDecimal.ONE);
        moneyTransferRestClient.transfer(String.valueOf(TWO), String.valueOf(ONE), BigDecimal.ONE);

        AssertSupport.assertBalance(moneyTransferRestClient.getAccount(String.valueOf(ONE)).getBalance());
        AssertSupport.assertBalance(moneyTransferRestClient.getAccount(String.valueOf(TWO)).getBalance());
    }

}
