package com.revolut.demo.services.moneytransfers.support;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.demo.services.moneytransfers.model.Account;
import com.revolut.demo.services.moneytransfers.model.Balance;
import com.revolut.demo.services.moneytransfers.model.json.TransferRequest;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.eclipse.jetty.http.HttpStatus;
import spark.utils.IOUtils;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.URISyntaxException;

public class MoneyTransferRestClient {

    private static final HttpHost host = HttpHost.create("http://localhost:4567");
    private final CloseableHttpClient httpClient = HttpClients.createDefault();
    private final ObjectMapper objectMapper = ObjectMapperConfiguration.getNewInstant()
            .addMixIn(Account.class, AccountMixInForTest.class)
            .addMixIn(Balance.class, BalanceMixIn.class);

    public Integer createAccount(String name) throws IOException, RestResponseException {
        HttpPost httpPost = new HttpPost("/accounts");
        httpPost.setEntity(new StringEntity(name, ContentType.APPLICATION_JSON));

        return httpClient.execute(host, httpPost, response -> {
            if (response.getStatusLine().getStatusCode() == HttpStatus.CREATED_201) {
                return objectMapper.readValue(response.getEntity().getContent(), Integer.class);
            } else {
                throw new RestResponseException(
                        response.getStatusLine().getStatusCode(),
                        response.getStatusLine().getReasonPhrase(),
                        objectMapper.readTree(response.getEntity().getContent()).asText()
                );
            }
        });
    }

    public Account getAccount(String account) throws IOException, URISyntaxException, RestResponseException {
        HttpGet httpGet = new HttpGet(new URIBuilder().setPathSegments("accounts", account).build());
        return httpClient.execute(host, httpGet, response -> {
            if (response.getStatusLine().getStatusCode() == HttpStatus.OK_200) {
                return objectMapper.readValue(response.getEntity().getContent(), Account.class);
            } else {
                StringWriter writer = new StringWriter();
                IOUtils.copy(response.getEntity().getContent(), writer);
                throw new RestResponseException(
                        response.getStatusLine().getStatusCode(),
                        response.getStatusLine().getReasonPhrase(),
                        writer.toString()
                );
            }
        });
    }

    public void charge(String account, BigDecimal amount) throws IOException, URISyntaxException, RestResponseException {
        String body = objectMapper.writeValueAsString(amount);
        HttpPost httpPost = new HttpPost(new URIBuilder().setPathSegments("accounts", account, "operations", "charge").build());
        httpPost.setEntity(new StringEntity(body, ContentType.APPLICATION_JSON));

        httpClient.execute(host, httpPost, response -> {
            if (response.getStatusLine().getStatusCode() != HttpStatus.OK_200) {
                StringWriter writer = new StringWriter();
                IOUtils.copy(response.getEntity().getContent(), writer);
                throw new RestResponseException(
                        response.getStatusLine().getStatusCode(),
                        response.getStatusLine().getReasonPhrase(),
                        writer.toString()
                );
            }
            return HttpStatus.OK_200;
        });
    }

    public void withdraw(String account, BigDecimal amount) throws IOException, URISyntaxException, RestResponseException {
        String body = objectMapper.writeValueAsString(amount);
        HttpPost httpPost = new HttpPost(new URIBuilder().setPathSegments("accounts", account, "operations", "withdraw").build());
        httpPost.setEntity(new StringEntity(body, ContentType.APPLICATION_JSON));

        httpClient.execute(host, httpPost, response -> {
            if (response.getStatusLine().getStatusCode() != HttpStatus.OK_200) {
                StringWriter writer = new StringWriter();
                IOUtils.copy(response.getEntity().getContent(), writer);
                throw new RestResponseException(
                        response.getStatusLine().getStatusCode(),
                        response.getStatusLine().getReasonPhrase(),
                        writer.toString()
                );
            }
            return HttpStatus.OK_200;
        });
    }

    public void transfer(String accountSource, String accountDestination, BigDecimal amount) throws IOException, URISyntaxException, RestResponseException {
        String body = objectMapper.writeValueAsString(new TransferRequest(Integer.parseInt(accountDestination), amount));
        HttpPost httpPost = new HttpPost(new URIBuilder().setPathSegments("accounts", accountSource, "operations", "transfer").build());
        httpPost.setEntity(new StringEntity(body, ContentType.APPLICATION_JSON));

        httpClient.execute(host, httpPost, response -> {
            if (response.getStatusLine().getStatusCode() != HttpStatus.OK_200) {
                StringWriter writer = new StringWriter();
                IOUtils.copy(response.getEntity().getContent(), writer);
                throw new RestResponseException(
                        response.getStatusLine().getStatusCode(),
                        response.getStatusLine().getReasonPhrase(),
                        writer.toString()
                );
            }
            return HttpStatus.OK_200;
        });
    }


    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    public abstract static class AccountMixInForTest extends Account {

        public AccountMixInForTest(@JsonProperty(value = "accountId") int integer,
                                   @JsonProperty(value = "name") String name) {
            super(integer, name);
        }


    }

    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    public abstract class BalanceMixIn extends Balance {

    }

}
