package com.revolut.demo.services.moneytransfers;

import com.revolut.demo.services.moneytransfers.service.AccountService;
import com.revolut.demo.services.moneytransfers.service.impl.AccountServiceImpl;
import org.junit.Test;

import java.util.NoSuchElementException;

public class AccountService_Failed_Test {

    private static final AccountService accountService = new AccountServiceImpl();

    @Test(expected = IllegalArgumentException.class)
    public void name() {
        accountService.createAccount(null);
    }

    @Test(expected = NoSuchElementException.class)
    public void notFoundAccountId() {
        accountService.getAccount(Integer.MAX_VALUE);
    }

}
