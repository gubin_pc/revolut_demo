package com.revolut.demo.services.moneytransfers.support;

public class RestResponseException extends RuntimeException {

    private final int statusCode;
    private final String reasonPhrase;
    private final String content;

    public RestResponseException(int statusCode, String reasonPhrase, String content) {

        this.statusCode = statusCode;
        this.reasonPhrase = reasonPhrase;
        this.content = content;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getReasonPhrase() {
        return reasonPhrase;
    }

    public String getContent() {
        return content;
    }
}
