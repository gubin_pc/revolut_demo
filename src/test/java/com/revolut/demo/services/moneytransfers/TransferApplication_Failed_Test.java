package com.revolut.demo.services.moneytransfers;

import com.revolut.demo.services.moneytransfers.support.MoneyTransferRestClient;
import com.revolut.demo.services.moneytransfers.support.RestResponseException;
import org.eclipse.jetty.http.HttpStatus;
import org.hamcrest.CustomTypeSafeMatcher;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import spark.Spark;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;

public class TransferApplication_Failed_Test {

    private static final MoneyTransferRestClient moneyTransferRestClient = new MoneyTransferRestClient();
    private static int ONE;
    private static int TWO;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @BeforeClass
    public static void beforeClass() throws Exception {
        MoneyTransferApplication.main(null);
        Spark.awaitInitialization();
        ONE = moneyTransferRestClient.createAccount("one");
        TWO = moneyTransferRestClient.createAccount("two");
    }

    @Test
    public void notFoundAccountId() throws IOException, URISyntaxException {
        //not found account Id
        exception.expect(new RestResponseExceptionMatcher(HttpStatus.NOT_FOUND_404));
        moneyTransferRestClient.getAccount(String.valueOf(Integer.MAX_VALUE));
    }

    @Test
    public void badAccountId() throws IOException, URISyntaxException {
        //bad account Id
        exception.expect(new RestResponseExceptionMatcher(HttpStatus.BAD_REQUEST_400));
        moneyTransferRestClient.getAccount("badId");
    }

    @Test
    public void badAmountForCharge() throws IOException, URISyntaxException {
        //bad amount
        exception.expect(new RestResponseExceptionMatcher(HttpStatus.BAD_REQUEST_400));
        moneyTransferRestClient.charge(String.valueOf(ONE), null);
    }

    @Test
    public void negativeAmountForCharge() throws IOException, URISyntaxException {
        //negative amount
        exception.expect(new RestResponseExceptionMatcher(HttpStatus.BAD_REQUEST_400));
        moneyTransferRestClient.charge(String.valueOf(ONE), BigDecimal.ONE.negate());
    }

    @Test
    public void badAmountForWithdraw() throws IOException, URISyntaxException {
        //bad amount
        exception.expect(new RestResponseExceptionMatcher(HttpStatus.BAD_REQUEST_400));
        moneyTransferRestClient.withdraw(String.valueOf(ONE), null);
    }

    @Test
    public void lessThenZeroAfterWithdraw() throws IOException, URISyntaxException {
        //less then Zero after decrease
        exception.expect(new RestResponseExceptionMatcher(HttpStatus.BAD_REQUEST_400));
        moneyTransferRestClient.withdraw(String.valueOf(ONE), BigDecimal.TEN);
    }

    @Test
    public void negativeAmountForWithdraw() throws IOException, URISyntaxException {
        //negative amount
        exception.expect(new RestResponseExceptionMatcher(HttpStatus.BAD_REQUEST_400));
        moneyTransferRestClient.withdraw(String.valueOf(ONE), BigDecimal.ONE.negate());
    }

    @Test
    public void negativeAmountForTransfer() throws IOException, URISyntaxException {
        //negative amount
        exception.expect(new RestResponseExceptionMatcher(HttpStatus.BAD_REQUEST_400));
        moneyTransferRestClient.transfer(String.valueOf(ONE), String.valueOf(TWO), BigDecimal.ONE.negate());
    }

    @Test
    public void lessThenZeroAfterTransfer() throws IOException, URISyntaxException {
        //less then Zero after decrease
        exception.expect(new RestResponseExceptionMatcher(HttpStatus.BAD_REQUEST_400));
        moneyTransferRestClient.transfer(String.valueOf(ONE), String.valueOf(TWO), BigDecimal.ONE);
    }

    @Test
    public void sameAccountForTransfer() throws IOException, URISyntaxException {
        //same account for transfer
        exception.expect(new RestResponseExceptionMatcher(HttpStatus.BAD_REQUEST_400));
        moneyTransferRestClient.transfer(String.valueOf(ONE), String.valueOf(ONE), BigDecimal.ONE);

    }

    private static class RestResponseExceptionMatcher extends CustomTypeSafeMatcher<RestResponseException> {

        private final int expectedHttpStatus;

        public RestResponseExceptionMatcher(int expectedHttpStatus) {
            super("Assert http status code");
            this.expectedHttpStatus = expectedHttpStatus;
        }

        @Override
        protected boolean matchesSafely(RestResponseException restResponseException) {
            return expectedHttpStatus == restResponseException.getStatusCode();
        }

    }

}