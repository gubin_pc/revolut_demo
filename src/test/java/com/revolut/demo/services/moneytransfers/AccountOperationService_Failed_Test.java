package com.revolut.demo.services.moneytransfers;

import com.revolut.demo.services.moneytransfers.service.AccountOperationService;
import com.revolut.demo.services.moneytransfers.service.impl.AccountOperationServiceImpl;
import com.revolut.demo.services.moneytransfers.storage.AccountInMemoryStorage;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

public class AccountOperationService_Failed_Test {

    private static final AccountOperationService accountOperationService = new AccountOperationServiceImpl();
    private static int ONE;
    private static int TWO;

    @BeforeClass
    public static void beforeClass() {
        ONE = AccountInMemoryStorage.add("one");
        TWO = AccountInMemoryStorage.add("two");
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeAmountForCharge() {
        accountOperationService.charge(ONE, BigDecimal.ONE.negate());
    }

    @Test(expected = IllegalArgumentException.class)
    public void lessThenZeroAfterWithdraw() {
        accountOperationService.withdraw(ONE, BigDecimal.TEN);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeAmountForWithdraw() {
        accountOperationService.withdraw(ONE, BigDecimal.ONE.negate());
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeAmountForTransfer() {
        accountOperationService.transfer(ONE, TWO, BigDecimal.ONE.negate());
    }

    @Test(expected = IllegalArgumentException.class)
    public void lessThenZeroAfterTransfer() {
        accountOperationService.transfer(ONE, TWO, BigDecimal.ONE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void sameAccountForTransfer() {
        accountOperationService.transfer(ONE, ONE, BigDecimal.ONE);

    }
}
