package com.revolut.demo.services.moneytransfers;

import com.revolut.demo.services.moneytransfers.model.Account;
import com.revolut.demo.services.moneytransfers.service.AccountOperationService;
import com.revolut.demo.services.moneytransfers.service.AccountService;
import com.revolut.demo.services.moneytransfers.service.impl.AccountOperationServiceImpl;
import com.revolut.demo.services.moneytransfers.service.impl.AccountServiceImpl;
import org.eclipse.jetty.util.BlockingArrayQueue;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class AccountOperationService_Stress_Test {

    private static final int OPERATIONS_TASK_AMOUNT = 500;
    private static final int OPERATION_TYPES = 3; //charge, withdraw, transfer
    private static final int ACCOUNTS_AMOUNT = 3; //charge, withdraw, transfer
    private static final int THREAD_POOL_SIZE = 10;

    @Test
    public void stressTest() throws InterruptedException {
        AccountOperationService accountOperationService = new AccountOperationServiceImpl();
        AccountService accountService = new AccountServiceImpl();

        final CountDownLatch readyForStart = new CountDownLatch(1);
        CountDownLatch alreadyFinished = new CountDownLatch(OPERATIONS_TASK_AMOUNT * OPERATION_TYPES * ACCOUNTS_AMOUNT);

        int[] accounts = new int[ACCOUNTS_AMOUNT];

        for (int i = 0; i < ACCOUNTS_AMOUNT; i++) {
            int id = accountService.createAccount(String.valueOf(i));
            accountOperationService.charge(id, new BigDecimal(5000));
            accounts[i] = id;
        }


        BlockingQueue<Runnable> runnables = new BlockingArrayQueue<>();
        for (int i = 0; i < OPERATIONS_TASK_AMOUNT; i++) {
            for (int j = 0; j < ACCOUNTS_AMOUNT; j++) {
                final int k = j;
                final int next = (k + 1) % ACCOUNTS_AMOUNT;
                runnables.add(() -> withLatches(v -> accountOperationService.charge(accounts[k], BigDecimal.TEN), readyForStart, alreadyFinished));
                runnables.add(() -> withLatches(v -> accountOperationService.withdraw(accounts[k], BigDecimal.valueOf(9)), readyForStart, alreadyFinished));
                runnables.add(() -> withLatches(v -> accountOperationService.transfer(accounts[k], accounts[next], BigDecimal.ONE), readyForStart, alreadyFinished));
            }
        }

        System.out.println(runnables.size());
        System.out.println(alreadyFinished.getCount());

        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        while (true) {
            Runnable poll = runnables.poll();
            if (poll == null) {
                readyForStart.countDown();
                break;
            }
            executorService.submit(poll);
        }

        alreadyFinished.await();

        for (int i = 0; i < ACCOUNTS_AMOUNT; i++) {
            final int next = (i + 1) % ACCOUNTS_AMOUNT;
            Account account = accountService.getAccount(accounts[i]);
            Account nextAccount = accountService.getAccount(accounts[next]);
            Assert.assertEquals(account.getBalance().getBalanceAmount(), nextAccount.getBalance().getBalanceAmount());

            //[(TYPE_OPERATIONS + 1) + 1] --- 2 record as charge, withdraw and 2 record for transfer and one at the beginning charge
            int expectedBalanceHistoryAmount = OPERATIONS_TASK_AMOUNT * (OPERATION_TYPES + 1) + 1;
            Assert.assertEquals(expectedBalanceHistoryAmount, account.getBalance().getBalanceHistories().size());
        }
    }

    void withLatches(Consumer<Void> consumer, CountDownLatch readyForStart, CountDownLatch alreadyFinished) {
        try {
            readyForStart.await();
            consumer.accept(null);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            alreadyFinished.countDown();
        }
    }

}