package com.revolut.demo.services.moneytransfers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.demo.services.moneytransfers.model.Account;
import com.revolut.demo.services.moneytransfers.model.BalanceHistory;
import com.revolut.demo.services.moneytransfers.support.ObjectMapperConfiguration;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Instant;

public class SerializeAccountTest {

    private final ObjectMapper objectMapper = ObjectMapperConfiguration.getNewInstant();

    @Test
    public void serializeAccountTest() throws JsonProcessingException {
        Account account = new Account(1, "name");
        account.getBalance().setBalance(BigDecimal.TEN);
        account.getBalance().addBalanceHistory(new BalanceHistory(BigDecimal.TEN, Instant.now(), "description"));
        String string = objectMapper.writeValueAsString(account);

        System.out.println(string);
        JsonNode jsonNode = objectMapper.readTree(string);
        Assert.assertEquals("name", jsonNode.findValue("name").asText());
        Assert.assertEquals(1, jsonNode.findValue("accountId").asInt());
        Assert.assertEquals(10, jsonNode.findValue("amount").asInt());
        Assert.assertEquals("description", jsonNode.findValue("description").asText());
        Assert.assertNotNull(jsonNode.findValue("operationTime").asText());
        Assert.assertNotNull(jsonNode.findValue("balance").asText());
    }
}