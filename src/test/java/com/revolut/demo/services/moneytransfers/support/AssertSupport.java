package com.revolut.demo.services.moneytransfers.support;

import com.revolut.demo.services.moneytransfers.model.Account;
import com.revolut.demo.services.moneytransfers.model.Balance;
import com.revolut.demo.services.moneytransfers.model.BalanceHistory;
import org.junit.Assert;

import java.math.BigDecimal;

public class AssertSupport {

    public static void assertAfterCreate(int accountId, String name, Account account) {
        Assert.assertEquals(accountId, account.getAccountId());
        Assert.assertEquals(name, account.getName());
        Assert.assertNotNull(account.getBalance());
        assertBalance(account.getBalance());
    }

    public static void assertBalance(Balance balance) {
        Assert.assertNotNull(balance);
        if (balance.getBalanceHistories().isEmpty()) {
            Assert.assertEquals(BigDecimal.ZERO, balance.getBalanceAmount());
            Assert.assertTrue(balance.getBalanceHistories().isEmpty());
        } else {
            BigDecimal balanceAmount = balance.getBalanceAmount();
            BigDecimal evaluateBalance = balance.getBalanceHistories().stream()
                    .map(BalanceHistory::getAmount)
                    .reduce(BigDecimal::add)
                    .orElse(BigDecimal.ZERO);
            Assert.assertEquals(balanceAmount, evaluateBalance);
        }

    }
}
