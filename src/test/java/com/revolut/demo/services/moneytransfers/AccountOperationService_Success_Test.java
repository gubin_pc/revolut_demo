package com.revolut.demo.services.moneytransfers;

import com.revolut.demo.services.moneytransfers.service.AccountOperationService;
import com.revolut.demo.services.moneytransfers.service.impl.AccountOperationServiceImpl;
import com.revolut.demo.services.moneytransfers.storage.AccountInMemoryStorage;
import com.revolut.demo.services.moneytransfers.support.AssertSupport;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

public class AccountOperationService_Success_Test {

    private static final AccountOperationService accountOperationService = new AccountOperationServiceImpl();
    private static int ONE;
    private static int TWO;

    @BeforeClass
    public static void beforeClass() {
        ONE = AccountInMemoryStorage.add("one");
        TWO = AccountInMemoryStorage.add("two");
    }

    @Test
    public void chargeOperation() {
        accountOperationService.charge(ONE, new BigDecimal("0.1"));
        AssertSupport.assertBalance(AccountInMemoryStorage.find(ONE).getBalance());
    }

    @Test
    public void withdrawOperation() {
        accountOperationService.charge(ONE, new BigDecimal("0.1"));
        AssertSupport.assertBalance(AccountInMemoryStorage.find(ONE).getBalance());
    }

    @Test
    public void transferOperation() {
        accountOperationService.charge(ONE, BigDecimal.ONE);

        accountOperationService.transfer(ONE, TWO, BigDecimal.ONE);
        accountOperationService.transfer(TWO, ONE, BigDecimal.ONE);
        AssertSupport.assertBalance(AccountInMemoryStorage.find(ONE).getBalance());
        AssertSupport.assertBalance(AccountInMemoryStorage.find(TWO).getBalance());
    }

}
