package com.revolut.demo.services.moneytransfers;

import com.revolut.demo.services.moneytransfers.model.Account;
import com.revolut.demo.services.moneytransfers.service.AccountService;
import com.revolut.demo.services.moneytransfers.service.impl.AccountServiceImpl;
import com.revolut.demo.services.moneytransfers.support.AssertSupport;
import org.junit.Test;

public class AccountService_Success_Test {

    private static final AccountService accountService = new AccountServiceImpl();

    @Test
    public void createAndGetAccount() {
        int ONE = accountService.createAccount("one");
        int TWO = accountService.createAccount("two");

        Account accountOne = accountService.getAccount(ONE);
        Account accountTwo = accountService.getAccount(TWO);

        AssertSupport.assertAfterCreate(ONE, "one", accountOne);
        AssertSupport.assertAfterCreate(TWO, "two", accountTwo);
    }

}
